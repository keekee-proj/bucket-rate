###Description
This application sets the speed of processing messages rabbitMQ.

###Examples

####Build docker image
```
docker build -t bucket-rate .
```
###Set configuration params
```yaml
#etc/conf.yaml
app:
  listen_address: :8883             # Application port
  authKey: J0h12oeuoII**yiu12       # Authorization key, must be passed in the headers of the http-request  
db:
  dsn: 192.168.99.100:32019         # Database dsn (MongoDB)  
  name: bucket_rate                 # Database name
rabbit:
  dsn: amqp://guest:guest@192.168.99.100:30930/ # RabbitMQ dsn
```

###Run application
```
docker run -it -e config=etc/conf.yaml -p 8884:8883 bucket-rate
```

###Create rate configuration
```
curl -i -H "authKey:J0h12oeuoII**yiu12" -X POST http://localhost:8884/task --data @test.json
HTTP/1.1 200 OK
{
  "result": "ok"
}

------------------------------test.json------------------------------
{
  "name": "test",
  "rate": 22,
  "queues": {
    "exchange": "buket_test",
    "input": {
      "route": "buket.input",
      "name": "buket_input"
    },
    "out": {
      "route": "buket.out",
      "name": "buket_out"
    }
  }
}
```

###Delete configuration
```
curl -i -H "authKey:J0h12oeuoII**yiu12" -X DELETE http://localhost:8884/task/test
HTTP/1.1 200 OK
{
  "result": "ok"
}
```

###Update value of rate for task
```
curl -i -H "authKey:J0h12oeuoII**yiu12" -X PUT http://localhost:8884/task/test --data @rate.json
HTTP/1.1 200 OK
{
  "result": "ok"
}

------------------------------rate.json------------------------------
{
  "rate": 22
}
```

###Get info by name
```
curl -i -H "authKey:J0h12oeuoII**yiu12" -X GET http://localhost:8884/task/test
HTTP/1.1 200 OK
{
  "result": {
      "name": "test",
      "rate": 22,
      "queues": {
        "exchange": "buket_test",
        "input": {
          "route": "buket.input",
          "name": "buket_input"
        },
        "out": {
          "route": "buket.out",
          "name": "buket_out"
        }
      },
      "info": {
        "startAt": "2018-02-19T16:17:00.611432637Z",
        "stopAt": "0001-01-01T00:00:00Z",
        "status": "alive",
        "error": "",
        "runCtn": 1
      }
    }
}
```

###Get info for all tasks
```
curl -i -H "authKey:J0h12oeuoII**yiu12" -X GET http://localhost:8884/tasks
HTTP/1.1 200 OK
{
  "result": [
    {
      "name": "test",
      "rate": 22,
      "queues": {
        "exchange": "buket_test",
        "input": {
          "route": "buket.input",
          "name": "buket_input"
        },
        "out": {
          "route": "buket.out",
          "name": "buket_out"
        }
      },
      "info": {
        "startAt": "2018-02-19T16:17:00.611432637Z",
        "stopAt": "0001-01-01T00:00:00Z",
        "status": "alive",
        "error": "",
        "runCtn": 1
      }
    }
  ]
}
```