package main

import (
	"flag"
	"log"
	"io/ioutil"
	"bitbucket.org/keekee-proj/bucket-rate/src/config"
	"bitbucket.org/keekee-proj/bucket-rate/src/http/server"
	"bitbucket.org/keekee-proj/bucket-rate/src/http/routes"
	"os"
	"os/signal"
	"net/http"
	"syscall"
	application "bitbucket.org/keekee-proj/bucket-rate/src/app"
)

var (
	confPath = flag.String("config", "", "Config file path")
)

func init() {
	flag.Parse()
	if *confPath == "" {
		log.Fatalf("config not set")
	}
	fData, err := ioutil.ReadFile(*confPath)
	if err != nil {
		log.Fatalf("Can not open file by path: [%s]", *confPath)
	}
	err = config.ReadFromFile(fData)
	if err != nil {
		log.Fatalf("Can not read file by path: [%s]", *confPath)
	}
}

func main() {
	chSignal := make(chan os.Signal)
	chStop := make(chan interface{})
	chFinish := make(chan interface{})
	signal.Notify(chSignal, syscall.SIGINT, syscall.SIGTERM)
	s := server.New(routes.RoutesList)
	go mapChannels(chSignal, chStop)
	go s.ListenStopCh(chStop, chFinish)
	cfg := config.Get()
	app, err := application.New()
	if err != nil {
		log.Fatalf("failed run app: [%s]", err)
	}
	err = app.LoadTasks()
	if err != nil {
		log.Fatalf("failed load programs: [%s]", err)
	}
	app.RunAll()
	log.Printf("start listen [%s]", cfg.App.ListenAddress)
	go http.ListenAndServe(cfg.App.ListenAddress, s)
	<-chFinish
	err = app.StopAll()
	if err != nil {
		log.Fatalf("error on stopping app: [%s]", err)
	}
}

func mapChannels(chSig <-chan os.Signal, chStop chan<- interface{}) {
	sig := <-chSig
	log.Printf("take signal: [%s:%d]", sig, sig)
	chStop <- true
}
