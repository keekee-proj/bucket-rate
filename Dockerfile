FROM golang:1.10-alpine as builder

ENV PROJECT_PATH ${GOPATH}/src/bitbucket.org/keekee-proj/bucket-rate

COPY ./ ${PROJECT_PATH}

WORKDIR ${PROJECT_PATH}
RUN apk add --no-cache \
        git \
        make
RUN go get -u github.com/golang/dep/cmd/dep\
    && dep ensure

RUN go build -o /app/bucket-rate cmd/main.go



FROM alpine:latest

WORKDIR /app

COPY --from=builder /app/bucket-rate /app/bucket-rate

EXPOSE 8883

CMD ./bucket-rate --config=${config}