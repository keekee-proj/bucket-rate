IMAGE=bucket-rate
PORT=8884
CONFIG=etc/conf.yaml

all: help

.PHONY: build run test


build:
	docker build -t $(IMAGE) .

run:
	docker run -it -e config=/bucket-rate/$(CONFIG) -p $(PORT):8883 -v $(shell pwd)/etc:/bucket-rate/etc  $(IMAGE)

help:
	@echo "help - show this help"
	@echo "build - build docker image"
	@echo "run - run docker image"