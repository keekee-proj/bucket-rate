package storage

import (
	"gopkg.in/mgo.v2"
	"time"
	"log"
)

type Storage struct {
	session *mgo.Session
	db      *mgo.Database
	dsn     string
	dbName  string
}

func New(dsn string, dbName string, keepAlive bool) (*Storage, error) {
	ses, err := mgo.Dial(dsn)
	if err != nil {
		return nil, err
	}
	db := ses.DB(dbName)
	s := &Storage{
		ses,
		db,
		dsn,
		dbName,
	}
	if keepAlive {
		go s.keepAlive()
	}
	return s, nil
}

func (s *Storage) DB() *mgo.Database {
	return s.db
}

func (s *Storage) keepAlive() {
	tik := time.Tick(100 * time.Millisecond)
	for range tik {
		err := s.session.Ping()
		if err != nil {
			ses, err := mgo.Dial(s.dsn)
			if err != nil {
				log.Print("failed reconnect to db")
				continue
			}
			s.session = ses
			s.db = ses.DB(s.dbName)
			log.Printf("successful reconnect db")
		}
	}
}

func (s *Storage) All(collection string, result interface{}) error {
	return s.db.C(collection).Find(nil).All(result)
}

func (s *Storage) OneId(collection string, id interface{}, result interface{}) error {
	return s.db.C(collection).FindId(id).One(result)
}

func (s *Storage) Create(collection string, doc interface{}) error {
	return s.db.C(collection).Insert(doc)
}

func (s *Storage) RemoveId(collection string, id interface{}) error {
	return s.db.C(collection).RemoveId(id)
}

func (s *Storage) UpdateId(collection string, id interface{}, doc interface{}) error {
	return s.db.C(collection).UpdateId(id, doc)
}
