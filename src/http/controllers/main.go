package controllers

import (
	"net/http"
	"bitbucket.org/keekee-proj/bucket-rate/src/app"
	"io/ioutil"
	"encoding/json"
	"errors"
)

func SetRate(response http.ResponseWriter, request *http.Request, pathParams map[string]interface{}) error {
	pName := pathParams["task"].(string)
	data, err := ioutil.ReadAll(request.Body)
	if err != nil {
		return err
	}
	type req struct {
		Rate int64 `json:"rate"`
	}
	jsonReq := &req{}

	if err = json.Unmarshal(data, jsonReq); err != nil {
		return err
	}

	if err = app.Get().SetRate(pName, jsonReq.Rate); err != nil {
		return err
	}

	response.Write(success)
	return nil
}

func AddTask(response http.ResponseWriter, request *http.Request, pathParams map[string]interface{}) error {
	data, err := ioutil.ReadAll(request.Body)
	if err != nil {
		return err
	}
	if len(data) == 0 {
		return errors.New("request empty")
	}
	t, err := makeTaskFromRequest(data)
	if err != nil {
		return err
	}
	ap := app.Get()

	if err = ap.Add(t); err != nil {
		return err
	}
	if err = ap.Run(t.Name); err != nil {
		return err
	}
	response.Write(success)
	return nil
}

func Task(response http.ResponseWriter, request *http.Request, pathParams map[string]interface{}) error {
	tName := pathParams["task"].(string)

	ap := app.Get()
	t, err := ap.Task(tName)
	if err != nil {
		return err
	}
	i, err := ap.TaskInfo(tName)
	if err != nil {
		return err
	}
	res, err := makeResponseTask(t, i)
	if err != nil {
		return err
	}
	response.Write(res)
	return nil
}

func Tasks(response http.ResponseWriter, request *http.Request, pathParams map[string]interface{}) error {
	ap := app.Get()

	ts := make([]*CTask, 0)

	for name, t := range ap.Tasks() {
		i, err := ap.TaskInfo(name)
		if err != nil {
			continue
		}
		ts = append(ts, makeTaskFromStruct(t, i))
	}
	res, err := makeResponseTasks(ts)
	if err != nil {
		return err
	}
	response.Write(res)
	return nil
}

func Delete(response http.ResponseWriter, request *http.Request, pathParams map[string]interface{}) error {
	pName := pathParams["task"].(string)

	ap := app.Get()
	err := ap.Remove(pName)
	if err != nil {
		return err
	}
	response.Write(success)
	return nil
}
