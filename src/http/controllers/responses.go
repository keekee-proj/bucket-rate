package controllers

import (
	"bitbucket.org/keekee-proj/bucket-rate/src/task"
	"encoding/json"
	"time"
	"bitbucket.org/keekee-proj/supervisor/program"
)

var success = []byte("{\"result\": \"ok\"}")

type queueConf struct {
	RouteKey  string `json:"route"`
	QueueName string `json:"name"`
}

type queues struct {
	Exchange string     `json:"exchange"`
	Input    *queueConf `json:"input"`
	Out      *queueConf `json:"out"`
}
type info struct {
	StartAt time.Time `json:"startAt"`
	StopAt  time.Time `json:"stopAt"`
	Status  string    `json:"status"`
	Error   string    `json:"error"`
	RunCtn  uint      `json:"runCtn"`
}
type CTask struct {
	Name   string  `json:"name"`
	Rate   int64   `json:"rate"`
	Queues *queues `json:"queues"`
	Info   *info   `json:"info"`
}

type response struct {
	Result interface{} `json:"result"`
}

func infoStatusMutator(status int) string {
	switch status {
	case 0:
		return "stopped"
	case 1:
		return "alive"
	case 2:
		return "stopping"
	default:
		return "undefined"
	}
}

func makeTaskFromRequest(r []byte) (*task.Task, error) {
	t := &CTask{}
	if err := json.Unmarshal(r, t); err != nil {
		return nil, err
	}

	return &task.Task{
		Name: t.Name,
		Rate: t.Rate,
		Queues: &task.Queues{
			Exchange: t.Queues.Exchange,
			Input: &task.QueueConf{
				RouteKey:  t.Queues.Input.RouteKey,
				QueueName: t.Queues.Input.QueueName,
			},
			Out: &task.QueueConf{
				RouteKey:  t.Queues.Out.RouteKey,
				QueueName: t.Queues.Out.QueueName,
			},
		},
	}, nil
}
func makeTaskFromStruct(t *task.Task, i *program.Info) *CTask {
	return &CTask{
		t.Name,
		t.Rate,
		&queues{
			t.Queues.Exchange,
			&queueConf{
				t.Queues.Input.RouteKey,
				t.Queues.Input.QueueName,
			},
			&queueConf{
				t.Queues.Out.RouteKey,
				t.Queues.Out.QueueName,
			},
		},
		&info{
			i.StartAt,
			i.StopAt,
			infoStatusMutator(i.Status),
			i.Error,
			i.RunCtn,
		},
	}
}
func makeResponseTask(t *task.Task, i *program.Info) ([]byte, error) {
	return json.Marshal(
		&response{makeTaskFromStruct(t, i)},
	)
}

func makeResponseTasks(ts []*CTask) ([]byte, error) {
	return json.Marshal(
		&response{ts},
	)
}
