package server

import (
	"log"
	"net/http"
	router "bitbucket.org/keekee-proj/router/v1"
	"sync"
	"time"
	"fmt"
)

type Server struct {
	sync.WaitGroup
	routes  *router.RouteList
	stopped bool
}

func (s *Server) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	t := time.Now()
	if s.stopped {
		res.WriteHeader(400)
		res.Write([]byte("Server closed"))
		return
	}
	s.Add(1)
	defer s.Done()
	route, params, err := s.routes.FindRoute(req.URL.Path, req.Method)
	if err != nil {
		if err == router.NOT_FOUND_ERROR {
			res.WriteHeader(404)
			res.Write([]byte("{\"error\": \"resource not found\"}"))
			return
		}
		res.WriteHeader(400)
		res.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err.Error())))
		return
	}

	err = route.Run(res, req, params)
	if err != nil {
		res.WriteHeader(400)
		res.Write([]byte(fmt.Sprintf("{\"error\": \"%s\"}", err.Error())))
		return
	}
	d := time.Now().Sub(t)
	log.Printf("duration: [%d] ns", d.Nanoseconds())
}

func New(routes *router.RouteList) *Server {
	return &Server{
		routes: routes,
	}
}

func (s *Server) ListenStopCh(stop <-chan interface{}, finished chan<- interface{}) {
	<-stop
	s.stopped = true
	s.Wait()
	finished <- true
}
