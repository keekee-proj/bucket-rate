package routes

import (
	router "bitbucket.org/keekee-proj/router/v1"
	"bitbucket.org/keekee-proj/bucket-rate/src/http/controllers"
	"bitbucket.org/keekee-proj/bucket-rate/src/http/middlewares"
)

var RoutesList = router.NewRouteList()

func init() {
	RoutesList.AddGroup(
		router.NewGroup("/task", middlewares.CheckKey, middlewares.CheckTask).AddRoutes(
			router.NewRoute(router.PUT, "/task:string", controllers.SetRate),
			router.NewRoute(router.GET, "/task:string", controllers.Task),
			router.NewRoute(router.DELETE, "/task:string", controllers.Delete),
		),
	)
	RoutesList.AddGroup(
		router.NewGroup("/", middlewares.CheckKey).AddRoutes(
			router.NewRoute(router.POST, "/task", controllers.AddTask),
			router.NewRoute(router.GET, "/tasks", controllers.Tasks),
		),
	)
}
