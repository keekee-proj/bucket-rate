package middlewares

import (
	"net/http"
	"bitbucket.org/keekee-proj/bucket-rate/src/config"
	"github.com/pkg/errors"
	"bitbucket.org/keekee-proj/bucket-rate/src/app"
)

func CheckKey(r *http.Request, params map[string]interface{}) error {
	key := config.Get().App.AuthKey
	if r.Header.Get("authKey") != key {
		return errors.New("auth failed")
	}
	return nil
}
func CheckTask(r *http.Request, params map[string]interface{}) error {
	t := params["task"].(string)
	if t == "" {
		return errors.New("task is empty")
	}
	application := app.Get()
	_, err := application.Task(t)
	return err
}
