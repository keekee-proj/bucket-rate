package config

import (
	"gopkg.in/yaml.v2"
)

type App struct {
	ListenAddress string `yaml:"listen_address"`
	AuthKey       string `yaml:"authKey"`
}

type Storage struct {
	Dsn  string `yaml:"dsn"`
	Name string `yaml:"name"`
}

type Amqp struct {
	Dsn string `yaml:"dsn"`
}

type Config struct {
	App  App     `yaml:"app"`
	Db   Storage `yaml:"db"`
	Amqp Amqp    `yaml:"rabbit"`
}

var conf = &Config{}

func ReadFromFile(file []byte) error {
	return yaml.Unmarshal(file, conf)
}

func Get() *Config {
	return conf
}
