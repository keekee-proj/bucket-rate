package task

import "fmt"

type QueueConf struct {
	RouteKey  string `bson:"route"`
	QueueName string `bson:"name"`
}

type Queues struct {
	Exchange string     `bson:"exchange"`
	Input    *QueueConf `bson:"input"`
	Out      *QueueConf `bson:"out"`
}

type Task struct {
	Name   string  `bson:"_id"`
	Rate   int64   `bson:"rate"`
	Queues *Queues `bson:"queues"`
}

func (p *Task) Validate() error {
	if p.Rate < 1 {
		return fmt.Errorf("rate must be greater than 0, setted [%d]", p.Rate)
	}
	if p.Queues.Input.QueueName == "" {
		return fmt.Errorf("value of queue name is not set")
	}
	if p.Queues.Input.RouteKey == "" {
		return fmt.Errorf("value of route key is not set")
	}
	if p.Queues.Input.QueueName == p.Queues.Out.QueueName {
		return fmt.Errorf("values of queue name must be different")
	}
	if p.Queues.Input.RouteKey == p.Queues.Out.RouteKey {
		return fmt.Errorf("values of route key must be different")
	}

	return nil
}
