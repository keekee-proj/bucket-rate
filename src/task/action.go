package task

import (
	"time"
	"github.com/streadway/amqp"
	"bitbucket.org/keekee-proj/supervisor/program"
	"log"
	"fmt"
)

func MakeAction(t *Task, con *amqp.Connection) program.Action {
	action := func(chStop <-chan bool) error {
		ch, err := con.Channel()
		if err != nil {
			return err
		}
		initQueues(ch, t.Queues)

		tik := time.Tick(time.Duration(int64(time.Minute) / t.Rate))
		for {
			select {
			case <-chStop:
				log.Printf("task stop")
				return nil
			case <-tik:
				msg, ok, err :=ch.Get(t.Queues.Input.QueueName, false)
				if err != nil {
					return err
				}
				if !ok {
					continue
				}
				fmt.Printf("message [%s]", msg.Body)
				err = publishMessage(ch, msg, t.Queues)
				if err != nil {
					msg.Nack(false, true)
					continue
				}
				msg.Ack(false)
			}
		}
	}
	return action
}

func publishMessage(ch *amqp.Channel, msg amqp.Delivery, q *Queues) error {
	outMessage := amqp.Publishing{
		Headers:         msg.Headers,
		ContentType:     msg.ContentType,
		ContentEncoding: msg.ContentEncoding,
		DeliveryMode:    msg.DeliveryMode,
		Priority:        msg.Priority,
		CorrelationId:   msg.CorrelationId,
		ReplyTo:         msg.ReplyTo,
		Expiration:      msg.Expiration,
		MessageId:       msg.MessageId,
		Timestamp:       msg.Timestamp,
		Type:            msg.Type,
		UserId:          msg.UserId,
		AppId:           msg.AppId,
		Body:            msg.Body,
	}
	return ch.Publish(
		q.Exchange,
		q.Out.RouteKey,
		false,
		false,
		outMessage,
	)
}

func initQueues(ch *amqp.Channel, q *Queues) error {
	err := ch.ExchangeDeclare(
		q.Exchange,
		amqp.ExchangeDirect,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}
	log.Printf("declare exchange [%s]", q.Exchange)

	_, err = ch.QueueDeclare(
		q.Input.QueueName,
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}
	log.Printf("declare input queue [%s]", q.Input.QueueName)

	_, err = ch.QueueDeclare(
		q.Out.QueueName,
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}
	log.Printf("declare out queue [%s]", q.Out.QueueName)

	err = ch.QueueBind(
		q.Input.QueueName,
		q.Input.RouteKey,
		q.Exchange,
		false,
		nil,
	)
	if err != nil {
		return err
	}
	log.Printf("bind ex[%s] r[%s] to q[%s]", q.Exchange, q.Input.RouteKey, q.Input.QueueName)

	err = ch.QueueBind(
		q.Out.QueueName,
		q.Out.RouteKey,
		q.Exchange,
		false,
		nil,
	)
	if err != nil {
		return err
	}
	log.Printf("bind ex[%s] r[%s] to q[%s]", q.Exchange, q.Out.RouteKey, q.Out.QueueName)

	return nil
}
