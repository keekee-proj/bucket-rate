package queue

import (
	"github.com/streadway/amqp"
	"fmt"
	"log"
	"time"
)

type Queue struct {
	amqp      *amqp.Connection
	keepAlive bool
	dsn       string
}

func New(dsn string, keepAlive bool) (*Queue, error) {
	rabbit, err := amqp.Dial(dsn)
	if err != nil {
		return nil, fmt.Errorf("amqp error: [%s]", err)
	}
	q := &Queue{
		rabbit,
		keepAlive,
		dsn,
	}
	if keepAlive {
		go keepAliveRabbit(q)
	}
	return q, nil
}

func keepAliveRabbit(q *Queue) {
	ch := make(chan *amqp.Error)
	for {
		q.amqp.NotifyClose(ch)
		err := <-ch
		log.Printf("amqp error: [%s, code: %d]", err.Reason, err.Code)
		if !q.keepAlive {
			break
		}
		con, e := amqp.Dial(q.dsn)
		if e != nil {
			tik := time.Tick(10 * time.Millisecond)
			for range tik {
				con, e = amqp.Dial(q.dsn)
				if e == nil {
					break
				}
			}
		}
		log.Printf("amqp successful reconnect")
		q.amqp = con
	}
}

func (q *Queue) Amqp() *amqp.Connection {
	return q.amqp
}
