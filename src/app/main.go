package app

import (
	"bitbucket.org/keekee-proj/bucket-rate/src/config"
	"bitbucket.org/keekee-proj/bucket-rate/src/storage"
	"bitbucket.org/keekee-proj/supervisor"
	"time"
	"bitbucket.org/keekee-proj/supervisor/errors"
	"bitbucket.org/keekee-proj/supervisor/program"
	"fmt"
	"bitbucket.org/keekee-proj/bucket-rate/src/queue"
	"bitbucket.org/keekee-proj/bucket-rate/src/task"
)

const (
	collectionTasks = "tasks"
	waitTimeout     = time.Minute
)

var app *App

type App struct {
	db         *storage.Storage
	queueCon   *queue.Queue
	supervisor *supervisor.Supervisor
	tasks      map[string]*task.Task
}

func Get() *App {
	return app
}
func New() (*App, error) {
	cfg := config.Get()
	db, err := storage.New(cfg.Db.Dsn, cfg.Db.Name, true)
	if err != nil {
		return nil, fmt.Errorf("storage error: [%s]", err)
	}
	q, err := queue.New(cfg.Amqp.Dsn, true)
	if err != nil {
		return nil, fmt.Errorf("amqp error: [%s]", err)
	}
	sv := supervisor.New()
	app = &App{
		db,
		q,
		sv,
		make(map[string]*task.Task),
	}
	return app, nil
}

func (app *App) LoadTasks() error {
	var tasks []*task.Task

	if err := app.db.All(collectionTasks, &tasks); err != nil {
		return err
	}

	for _, t := range tasks {
		app.tasks[t.Name] = t
		app.AddToSupervisor(t)
	}

	return nil
}

func (app *App) AddToSupervisor(t *task.Task) {
	action := task.MakeAction(t, app.queueCon.Amqp())
	app.supervisor.Add(t.Name, action)
}

func (app *App) RunAll() {
	app.supervisor.RunAll()
}

func (app *App) Add(t *task.Task) error {
	_, ok := app.tasks[t.Name]
	if ok {
		return fmt.Errorf("task [%s] already exists", t.Name)
	}

	if err := t.Validate(); err != nil {
		return err
	}

	if err := app.db.Create(collectionTasks, t); err != nil {
		return err
	}
	app.tasks[t.Name] = t
	app.AddToSupervisor(t)
	return nil
}

func (app *App) Remove(name string) error {
	t, ok := app.tasks[name]
	if !ok {
		return fmt.Errorf("undefined task [%s]", t.Name)
	}

	app.supervisor.Stop(name)

	if err := app.supervisor.WaitProgram(t.Name, waitTimeout); err != nil && err != errors.ERROR_UNDEFINED_PROGRAM {
		return fmt.Errorf("supervisor error on wait: [%s]", err)
	}

	if err := app.db.RemoveId(collectionTasks, t.Name); err != nil {
		return fmt.Errorf("storage error on remove task: [%s]", err)
	}
	delete(app.tasks, t.Name)
	return nil
}

func (app *App) StopAll() error {
	app.supervisor.StopAll()
	return app.supervisor.WaitTimeout(waitTimeout)
}

func (app *App) Stop(name string) error {
	if err := app.supervisor.Stop(name); err != nil {
		return err
	}
	return app.supervisor.WaitProgram(name, waitTimeout)
}

func (app *App) Run(name string) error {
	return app.supervisor.Run(name)
}

func (app *App) Task(name string) (*task.Task, error) {
	p, ok := app.tasks[name]
	if !ok {
		return nil, fmt.Errorf("undefined program [%s]", name)
	}
	return p, nil
}

func (app *App) TaskInfo(name string) (*program.Info, error) {
	return app.supervisor.Info(name)
}

func (app *App) Tasks() map[string]*task.Task {
	return app.tasks
}

func (app *App) SetRate(name string, rate int64) error {
	p, ok := app.tasks[name]
	if !ok {
		return fmt.Errorf("undefined task [%s]", name)
	}
	if err := app.Stop(name); err != nil {
		return err
	}
	if err := app.supervisor.WaitProgram(name, waitTimeout); err != nil {
		return err
	}

	p.Rate = rate

	if err := app.db.UpdateId(collectionTasks, name, p); err != nil {
		return err
	}
	if err := app.Run(name); err != nil {
		return err
	}

	return nil
}
